============= READ ME!! =============

1.	To run the program

	./alim-search.sh $1 $2 $3 $4
	
	where
	
	$1 - graph file
	$2 - heuristic file
	$3 - starting node's name
	$4 - goal node's name
	
2.	My program will run but will not print out partial paths
	as explained in my report due to the way my algorithm is
	constructed. Sorry for any incoveniences. It will still
	find the various solutions the graph may have.

3.	If the program does not run due to a few terminal errors, run
	these commands

	chmod 744 alim-search.sh
	sed -i -e 's/\r$//' alim-search.sh

4.	After running the previous troubleshooting commands,
	re-run the program

	./alim-search.sh $1 $2 $3 $4

	where
	
	$1 - graph file
	$2 - heuristic file
	$3 - starting node's name
	$4 - goal node's name